import sublime, sublime_plugin
import time, re

timestamp_re = re.compile("^[0-9]{10}$")
# Insert unix timestamp
class TimestampConverterCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		# Generate timestamp 
		timestamp = time.time()
		for region in self.view.sel():
			# Add timestamp to selected positions
			self.view.insert(edit, region.begin(), str(long(timestamp)))

	# Don't show the option to insert timestamp if a timestamp is selected
	def is_visible(self):
		for region in self.view.sel():
			if (timestamp_re.match(self.view.substr(region)) == None):
				return True
		return False

# Convert a unix timestamp to date
class TimestampDateConverterCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		for region in self.view.sel():
			# Show input bar with date string
			self.view.window().show_input_panel("Date:", datetime.datetime.fromtimestamp(int(self.view.substr(region))).strftime('%d-%m-%Y %H:%M:%S'), None, None, None);
			
	# Whether or not we should show the option to convert to date
	def is_visible(self):
		for region in self.view.sel():
			if (timestamp_re.match(self.view.substr(region))):
				return True
		return False
